`timescale 1ns / 1ps
module frequencyDivider_TB;

reg clk, Reset;
wire clkSend;


frequencyDivider uut (.clk(clk), .clkSend(clkSend), .Reset(Reset));

initial begin
	Reset = 0;
	#30
	Reset = 1;
	#50
	Reset = 0;
end


initial begin 
	clk = 0;
	forever begin
		#20 clk =~clk;
	end
end

initial begin: TEST_CASE
		$dumpfile("frequencyDivider_TB.vcd");
		$dumpvars(-1, uut);
		#2000000$finish;
end

endmodule 
