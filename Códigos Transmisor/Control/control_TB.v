`timescale 1ns / 1ps

module control_TB;

	reg Complete;
	reg NullString;
	reg NullDataChar;
	reg clk;
	
        wire Reset;	
	wire LoadString;
	wire CompareString;
	wire LoadNextChar;
	wire GenerateData;
	wire SendData;

control uut (.Complete(Complete), .NullString(NullString), .NullDataChar(NullDataChar), .LoadString(LoadString), .CompareString(CompareString), .LoadNextChar(LoadNextChar), .GenerateData(GenerateData), .SendData(SendData), .clk(clk), .Reset(Reset));

initial begin
	clk=0;
	forever begin
		#5 clk=~clk;
	end
end

initial begin

	Complete = 0;
	NullString = 0;
	NullDataChar = 0;
	#20
	Complete = 1;
	#70
	Complete = 0;
	NullDataChar = 1;
	NullString = 1;
	#20
	NullDataChar = 0;
	NullString = 0;
	
	

	//Complete=0;
	//NullString=0;
	//NullDataChar=0;
	//#200
	//NullDataChar=1;
	//#200
	//NullString=1;
	//Complete=1;
	//#200
	//NullString=0;
	//NullDataChar=0;
	//Complete=0;
end

   initial begin: TEST_CASE
     $dumpfile("control_TB.vcd");
     $dumpvars(-1, uut);
     #500 $finish;
   end

endmodule

