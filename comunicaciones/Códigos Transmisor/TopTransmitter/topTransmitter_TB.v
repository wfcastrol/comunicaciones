`timescale 1ns / 1ps
module topTransmitter_TB;

reg Complete, clk, Reset;
reg [0:76] StringPOV;
wire SendBit;


topTransmitter uut (.Complete(Complete), .clk(clk), .StringPOV(StringPOV), .SendBit(SendBit), .Reset(Reset));



initial begin 
	clk = 0;
	forever begin
		#10 clk =~clk;
	end
end

initial begin
	StringPOV = 77'b10110011011001101100110110011011001101100110110011011001101100110110011011001;
	Complete = 0;
	#310
	Complete= 1;
	#20
	Complete = ~Complete;
end


initial begin
	Reset = 0;
	#10
	Reset = 1;
	#20
	Reset = 0;
	
end

initial begin: TEST_CASE
		$dumpfile("topTransmitter_TB.vcd");
		$dumpvars(-1, uut);
		#15000000$finish;
end

endmodule 
