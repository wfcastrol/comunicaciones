`timescale 1ns / 1ps
module stringCharLoader_TB;

	reg [0:69] StringPOV;
	reg LoadString;
	reg LoadNextChar;
	reg clk;
	reg Reset;
	wire [0:69] String;
	wire [0:6] Char;

stringCharLoader uut ( .Char(Char), .StringPOV(StringPOV), .LoadString(LoadString), .LoadNextChar(LoadNextChar), .clk(clk), .Reset(Reset), .String(String) );

parameter OFFSET = 0;
parameter PERIOD          = 20;
parameter real DUTY_CYCLE = 0.5;

initial begin  
      clk = 0; 
end 

initial  begin  
     #OFFSET;
     forever begin
         clk = 1'b0;
         #(PERIOD-(PERIOD*DUTY_CYCLE)) clk = 1'b1;
         #(PERIOD*DUTY_CYCLE);
       end
end

initial begin	

	StringPOV=70'b1000100100010010001001000100100010010001001000100100010010001001000100;
	Reset=0;
	#20
	LoadString=1;
	LoadNextChar=0;

	#20
	LoadString=0;
	LoadNextChar=1;
	
	#20
	//StringPOV=70'b1000100100010010001001000100100010010001001000100111110010001001000100;
	LoadNextChar=1;
	
	
end

initial begin: TEST_CASE
		$dumpfile("stringCharLoader_TB.vcd");
		$dumpvars(-1, uut);
		#500 $finish;
end

endmodule



