module comparer ( String, CompareString, NullString, DataChar, NullDataChar);
input [0:76] String;
input CompareString;
output reg NullString;

input [0:9] DataChar;
output reg  NullDataChar;


always @(posedge CompareString) begin
	if (String == 0) NullString = 1;
	else NullString = 0;
end

always @(DataChar) begin
	if (DataChar == 0) NullDataChar = 1;
	else NullDataChar = 0;
end

endmodule
