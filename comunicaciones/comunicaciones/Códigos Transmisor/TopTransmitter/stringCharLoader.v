module stringCharLoader (StringPOV, LoadString, LoadNextChar, clk, Reset, String, Char);

input [0:76] StringPOV;
input LoadString;
input LoadNextChar;
input Reset;
input clk;
output reg [0:76] String;
output reg [0:6] Char;

always @(posedge clk)
	begin
		if (LoadString==1'b1)begin
				String<=StringPOV;
		end
		if (LoadNextChar==1'b1)begin
				Char<=String[0:6];
				//String<<7;
				String<={String[7:76],7'b0000000};
		end
	end

always @(posedge Reset) begin
	String<=0;
	Char<=0;
end
		
endmodule
