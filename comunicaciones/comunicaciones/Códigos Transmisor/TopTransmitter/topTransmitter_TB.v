`timescale 1ns / 1ps
module topTransmitter_TB;

reg Complete, clk, Reset;
reg [0:76] StringPOV;
wire SendBit;


topTransmitter uut (.Complete(Complete), .clk(clk), .StringPOV(StringPOV), .SendBit(SendBit), .Reset(Reset));


initial begin 
	clk = 0;
	forever begin
		#20 clk =~clk;
	end
end

initial begin
	Reset = 0;
	Complete = 0;
	StringPOV = {14'b01100111101010,63'b0};
	#30
	Complete = 1;
	#20
	Complete = 0;
end

initial begin: TEST_CASE
		$dumpfile("topTransmitter_TB.vcd");
		$dumpvars(-1, uut);
		#3000000$finish;
end

endmodule 
