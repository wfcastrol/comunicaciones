module frequencyDivider (clk, clkSend);

input clk, Reset;
output reg clkSend;


parameter period = 5208;
parameter n =12;
parameter halfPeriod = period/2;
reg [n:0] counter;


initial begin
	counter = 0;
	clkSend <= 0;
end

always @(posedge clk) begin
	
	if (counter == period-1) begin
		counter = 0;
		clkSend <= 0;
	end
	else counter = counter +1;
	if (counter == halfPeriod) clkSend <=1;
	
end

always @(posedge Reset) begin
	counter = 0;
	clkSend = 0;
end


endmodule
