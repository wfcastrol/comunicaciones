module parityCreator (Char, ParityBit);

input [0:6] Char;
output reg ParityBit;

always @(Char) begin
	ParityBit=(^ Char);	
end

endmodule
