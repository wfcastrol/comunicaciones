`timescale 1ns / 1ps
module parityCreator_TB;
reg [0:6] Char;
wire ParityBit;

parityCreator uut ( .Char(Char), .ParityBit(ParityBit) );

initial begin

	Char=7'b1101010;

	//parityCreator(Char,ParityBit);

end

initial begin: TEST_CASE
		$dumpfile("parityCreator_TB.vcd");
		$dumpvars(-1, uut);
		#500 $finish;
end

endmodule



