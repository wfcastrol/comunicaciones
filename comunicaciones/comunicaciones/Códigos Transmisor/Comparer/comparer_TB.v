`timescale 1ns / 1ps
module Comparer_tb;
reg [0:69] String;
reg CompareString;
wire NullString;

reg [0:9] DataChar;
wire NullDataChar;

comparer uut ( .String(String), .CompareString(CompareString), 
.NullString(NullString), .DataChar(DataChar), .NullDataChar(NullDataChar) );

initial begin
	String = 70'b1111000111100011110001111000111100011110001111000111100011110001111000;
	String = 0*String;
	CompareString = 0;
	DataChar = 0;
	#20
	CompareString = 1;
	#10
	String = 0;
	CompareString = 0;
	#20
	CompareString = 1;
	#20
	CompareString = 0;
	
	#20
	DataChar = 1000;
	#20
	DataChar = 0;		
end

initial begin: TEST_CASE
		$dumpfile("comparer_TB.vcd");
		$dumpvars(-1, uut);
		#500 $finish;
end
endmodule

