module bitSender(Char, ParityBit, GenerateData, SendData, Reset, clk, clkSend, SendBit, DataChar );

	input [0:6] Char;
	input ParityBit;
	input GenerateData;
	input SendData;
	input Reset;
	input clk;
	input clkSend;//delay to send the transmition bit
	output reg SendBit;
	output reg [0:9] DataChar;
	

initial begin
	SendBit <= 1;
end
	always@(posedge clk)begin
		if(GenerateData==1'b1)begin
			DataChar <= {1'b1,ParityBit,Char,1'b0 };
			SendBit <= 1'b1;
		end
	end	
	

	always@( clkSend )begin
		if(SendData == 1'b1)begin
			SendBit <= DataChar[9];
			DataChar <= {1'b0,DataChar[0:8]};
		end
		else SendBit <= 1;
	end

	always @(posedge Reset) begin
		DataChar <= 10'b0;
		SendBit <= 1;
	end



endmodule
