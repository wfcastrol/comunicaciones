`timescale 1ns / 1ps
module bitSender_TB;
	reg [0:6] Char;
	reg ParityBit;
	reg GenerateData;
	reg SendData;
	reg Reset;
	reg clk;
	reg clkSend;//delay to send the transmition bit
	wire SendBit;
	wire [0:9] DataChar; 

	bitSender uut ( .Char(Char), .ParityBit(ParityBit), .GenerateData(GenerateData), .SendData(SendData), .Reset(Reset), .clk(clk), .clkSend(clkSend), .SendBit(SendBit), .DataChar(DataChar));
	
	initial begin
		Reset = 0;		
		Char = 148;
		ParityBit = 1;

		clk = 0;
		clkSend =0;
	end

	initial begin
		forever begin	
			#20
			clk = ~clk;
		end
	end

	initial begin
		GenerateData=0;
		#30
		GenerateData=1;
		#40
		GenerateData=0;
	end
	initial begin
		SendData = 0;
		#60
		SendData=1;
		#1140000
		SendData=0;
		
	end

	initial forever begin
		#104000
		clkSend = ~clkSend;
	end
        
	initial begin: TEST_CASE
		$dumpfile("bitSender_TB.vcd");
		$dumpvars(-1, uut);
		#2040000 $finish;
	end

endmodule
