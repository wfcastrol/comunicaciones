# COMUNICACIONES #
---

 [TOC]

---

## **1. Marco teórico** ##

### **1.1 Comunicación serial** ###

#### *1.1.1 Códigos y palabras de código* ####

La representación en binarios es adecuada para el procesamiento interno de sistemas digitales. No obstante, el usuario puede preferir una interacción natural con el sistema, ingresando la información en un formato conocido para él (i.e, introduciendo un número en formato decimal). Por ello, un sistema digital debe tener una manera de establecer una correspondencia entre la representación del usuario y su equivalente en binario.

Cualquier conjunto de objetos -números, estados, palabras y demás- puede ser representado por *cadenas de bits* -bit strings-, en donde cada cadena de bits representa uno de los objetos del conjunto.

En este orden de ideas, un *código* es el conjunto de cadenas de *n*-bits que se utiliza para representar todos los elementos de un determinado conjunto.
Una *palabra de código* -code word- es una cadena de bits particular en el conjunto.

Un ejemplo sería el código ASCII, el cual representa las letras del abecedario en minúsculas y mayúsculas, además de los números decimales, signos de exclamación y caracteres de control. En total, cuenta con 128 palabras, es decir, 128 diferentes cadenas de código en binario (o hexadecimal).

#### *1.1.2 Códigos para corrección y detección de errores*: ####

Un *error* es el cambio que presenta un dato al pasar de su valor original a otro valor. Este proceso se denomina *corrupción de datos*. Los errores son causados por fallas físicas, las cuales pueden ser permanentes o temporales. 

Cuando el error afecta un único bit, se habla de un *error simple*. En el caso que varios bits se vean afectados, hablamos de un *error de múltiples bits*.

El esquema más fácil para estudiar los errores nos dice que cada falla física provoca únicamente un error. Así, cuando en una palabra de código se ven alterados varios bits, se concluye que se presentaron varias fallas físicas en el sistema.

Un *código de detección de errores* es aquel que tiene la propiedad de que ante la presencia de un error que afecte a una palabra de código, la cadena de bits resultante no va a pertenecer al código. Es decir, cambios en los bits debido a errores producirán una secuencia de bits que no tiene sentido para el sistema.

Un sistema que utilice códigos de detección de errores almacena únicamente palabras de código. Aquellos datos que no lo sean, automáticamente son marcados como poseedores de errores, y son descartados para futuros procesamientos.

* **Distancia**: La distancia es el mínimo número de bits que debe ser cambiado para pasar de un code word a otro en un determinado código. Entre mayor es la distancia entre code words, mejor es el código para detectar errores.

Por ejemplo, supongamos que tenemos un código en el cual tenemos únicamente dos elementos: "hola" y "chao". La codificación es la siguiente:

-> "hola" = 10010
-> "chao" = 1**1**01**1**

Los bits señalados en el code word de la palabra "chao" indican que para pasar de code word a code word en nuestro código, *se necesita un cambio en mínimo dos bits*, por lo cual la distancia del código es 2. Al transmitir los datos, por ejemplo "hola" = 10010, y presentarse un error, se recibe el código 10011, el cual no tiene sentido en nuestra codificación. De esta manera, el dato debe ser descartado.

* **Bit de paridad**:  El *bit de paridad* es un bit adicional que se asocia a una palabra de código, dependiendo del número de unos que tenga la palabra.

Consideremos la palabra de código 1001001 (número impar de 1s)

*Paridad par*: 0 = Número par de 1s en los bits de información.
               1 = en otro caso, número impar.

La palabra de código quedaría como 1001001**1**. Los siete primeros bits representan los bits de información. El bit de paridad puede situarse al principio o al fin de la palabra de código.

*Paridad impar*: 0 = Número impar de 1s en los bits de información.
                 1 = en otro caso, número par.

La palabra de código quedaría como 1001001**0**.

**Notemos que en la paridad par, el número total de 1s, incluyendo el bit de paridad, siempre es par. Lo mismo ocurre con la paridad impar.**

A continuación, mostramos un circuito con el cual podemos generar el bit de paridad par a una palabra de 4 bits:

![](http://perso.wanadoo.es/pictob/imagenes/genpar.gif)

La siguiente figura muestra cómo detectar si la paridad de una palabra de código de 5 bits es correcta. La palabra contiene 4 bits de información y 1 bit de paridad:

![](http://perso.wanadoo.es/pictob/imagenes/detpar.gif)

Las compuertas utilizadas son XOR.

Añadir un bit de paridad es la manera más sencilla de construir un código de detección de errores. Vale la pena resaltar que el bit de paridad está en la capacidad de detectar todos los cambios en un número *impar* de bits. Alterar dos bits no altera la paridad de la palabra de código.

Los códigos de detección y corrección de errores usan más de un bit de paridad, los cuales pasan a llamarse *check bits*. Algunos ejemplos son el Hamming code y los CRC codes. El código Hamming tiene la particularidad de que la distancia entre palabras de código es de 3, y posee la capacidad de corregir errores en un bit.

#### *1.1.3 Comunicación serial* ####

La comunicación serial es análoga a la forma en la que como seres humanos transmitimos mensajes el uno al otro: primero llamamos la atención del receptor, y luego hablamos, expresando la idea a transmitir palabra por palabra, sílaba a sílaba. La comunicación serial es entonces la transmisión de información bit a bit utilizando un único bus de datos (hilo) para el mensaje en cuestión [1].

Otro esquema de comunicación es el paralelo, en el cual cada bit de información cuenta con un hilo para su transmisión. 
Luego de ser almacenados, todos los bits de una palabra de código pueden leerse o escribirse de manera simultánea. Es por esto que el esquema resulta mucho más veloz, y es utilizado con frecuencia para la comunicación interna en los computadores, en especial para el almacenamiento de memoria. 

Sin embargo, la comunicación serial ofrece ciertas ventajas para el caso de la aplicación que estamos manejando (transmisión inalámbrica):

* Transmitir datos de manera serial no implica un número tan elevado de canales de comunicación. Generalmente, se utilizan de 2 a 4 hilos para la transmisión de *n* bits, mientras que en paralelo el requerimiento es de mínimo *n* hilos para la transmisión de *n* bits.
* Los datos pueden ser transmitidos a través de diferentes medios físicos: cables coaxiales, cobre, fibra óptica o medios inalámbricos: RF, infrarrojo, entre otros.
* El número y nivel de estándares de comunicación serial es tal vez su característica más importante. Algunos estándares sobresalientes son el USB y el RS232.

**Formato marca-espacio:** Utilizado en la comunicación serie en la cual se denominan dos niveles de tensión como dos valores lógicos.

* 1 lógico: Representa el estado de tensión llamado marca.
* 0 lógico: Representa el estado de tensión llamado espacio.

![](http://perso.wanadoo.es/pictob/imagenes/marcaespacio.gif)

#### *1.1.4 Consideraciones de la comunicación serial* ####

El sistema de codificación utilizado en la transmisión serial de datos debe tener un medio que permita la solución de los siguientes problemas:

* **Sincronización de bits**: El receptor necesita saber dónde comienza y dónde termina cada bit para realizar el muestreo del valor del bit en el centro del intervalo determinado por el tiempo de bit.
* **Sincronización del caracter:** La información serial se transmite bit a bit, pero su sentido puede estar determinado por la organización de esos bits en palabras, que generalmente son bytes -8 bits- de información.
* **Sincronización del mensaje:** Es necesario conocer el inicio y el fin de una cadena de caracteres por parte del receptor. Lo anterior es útil para la detección de errores en el mensaje.
* **Velocidad de transmisión**: Se expresa en bps -bits por segundo-. La velocidad de transmisión depende del ancho de banda, la potencia de la señal y el ruido que aparezca en el conductor de la señal. El reloj básicamente establece la velocidad de transmisión.

![](http://imageshack.us/scaled/landing/585/basicconceptsserialcom.png)

* **Base de reloj -clock CLK-:** En una comunicación es necesario establecer una base de tiempo que controle la velocidad. En un sistema digital, se puede utilizar la base de tiempos de reloj del sistema para este propósito, aunque existe la posibilidad de generar un reloj mediante un contador programable, a través de un oscilador o un dispositivo especializado que genera diferentes frecuencias de reloj.
El reloj define la tasa en la cual los bits son transmitidos. Se transmite un bit por ciclo de reloj.

* **Tasa de bits -bit rate-:** Se mide en bps y es igual a la frecuencia de CLK en ciclos por segundo [Hz].

* **Tiempo de bit -bit time-:** Es el recíproco de la tasa de bits. Representa el tiempo reservado en la línea para la transmisión de cada bit.

* **Celda de bit -bit cell-:** Tiempo ocupado por cada bit. 

* **Líneas o canales de comunicación:** Se pueden establecer canales para la comunicación de acuerdo a tres técnicas, las cuales dependen del hardware utilizado para la transmisión de información.

*Simplex*: También conocida como *unidireccional*. Existe un único transmisor y un único receptor, requiriendo así un solo hilo para los datos, lo cual llega a ser ventajoso en algunas situaciones.
La desventaja radica en que no hay ningún tipo de realimentación: el receptor no tiene forma de comunicarse con el transmisor para indicarle sobre su estado y sobre la calidad de la información que se recibe.

*Half-duplex*: La comunicación se establece en una sola línea, pero en ambos sentidos. Es decir, en un determinado instante de tiempo, el transmisor enviará información al receptor. En otro momento, se intercambian los roles de transmisor y receptor: el TX recibirá información del RX, quien, actuando como TX, enviará una información.
Lo más imporante de los canales half-duplex es que *no es posible transmitir información en ambos sentidos en forma simultánea*. Sin embargo, la realimentación permite desarrollar procedimientos de detección y corrección de errores.

*Full duplex*: Se utilizan dos hilos, uno transmisor y otro receptor. La información se transmite en ambos sentidos. La existencia de dos canales permite la transmisión y recepción de información en forma simultánea.

#### *1.1.5 Modos de comunicación* ####

Existen dos modos para realizar transmisión de datos de manera serial:

* Comunicación asíncrona: Los bits se transmiten utilizando bits o señales de ayuda que permiten mantener en entendimiento al transmisor y al receptor.
* Comunicación síncrona: Todos los bits de una palabra de código se envían consecutivamente, y las palabras se transmiten una tras otra. Se envía una señal de sincronización -SYNC- al inicio de cada palabra para mantener una constante sincronía entre transmisor y receptor. 

En la comunicación asíncrona, la velocidad de transmisión de datos debe ser previamente acordada y fijada en ambas partes. Un dato puede transmitirse en cualquier momento, por lo cual es útil mantener la línea en un estado de standby. Un nuevo dato debe transmitirse de tal manera que dicho estado de standby se vea roto.

---

### **1.2 Protocolo de comunicación asíncrono** ###

La comunicación serial asíncrona es un protocolo de comunicación serial en el cual una señal de entrada -start- se envía antes de las palabras de código que se van a transmitir, y una señal de parada -stop- se envía inmediatamente después.

El término *asíncrono* se utiliza puesto que la señal de start tiene como misión preparar al receptor para la transmisión. La señal de stop funciona para que el mecanismo de recepción se mantenga en un estado de reposo o standby a la espera de la siguiente palabra de código.

![](http://upload.wikimedia.org/wikipedia/commons/4/47/Puerto_serie_Rs232.png)

En el diagrama anterior, se transmitieron 16-bits de información (2 bytes). A cada bit, se le asocia un bit de start y un bit de stop, al principio y al final de la cadena. Entonces, se transmitió en total un string de 10-bits. Usualmente, se añade un bit de paridad adicional al final de la cadena. 

El número de los bits de datos, la codificación de la información, la presencia o ausencia de un bit de paridad y el tipo de la misma, y la velocidad de transmisión son aspectos que deben ser predefinidos entre ambos extremos de la línea de comunicación.

Es importante resaltar que el bit de stop generalmente no se utiliza como un solo bit, sino más bien como un "período de stop". Es decir, no es absolutamente necesario que siempre se envíe un sólo bit de stop, sino que pueden utilizarse varios para determinar un período de espera entre la recepción de dato y dato. Como es de esperarse, este período no debe ser menor que 2 tiempos de bit.

El receptor requiere un menor período de stop que el transmisor. Al final de cada caracter, el tiempo de espera es menor en el receptor que en el transmisor. Por ello, la comunicación asíncrona difiere de la síncrona, puesto que el transmisor y el receptor no se mantienen sincronizados.

Cuando no hay transmisión de datos, se dice que el estado de la línea es *idle*. Se transmite constantemente un uno lógico. En el momento en que se transmita un 0, el receptor inicia su proceso de preparación para la llegada de una nueva code word. 

---

### **1.3 ¿Qué es UART?** ###

UART:* **U**niversal **A**synchronous **R**eceiver/**T**ransmitter*. Es un elemento de hardware encargado de convertir datos de forma serial a paralela y viceversa. Las UARTs se utilizan conjuntamente con estándares de comunicación tales como el RS-232 y el RS-422.
La designación de *universal* se utiliza puesto que tanto el formato de los datos como la velocidad de transmisión son configurables. Los niveles eléctricos son controlados por un circuito externo a la UART.
Una UART es generalmente una parte de un circuito integrado especializado en comunicaciones seriales, que forma parte de un computador o del puerto serial de un periférico.

#### *1.3.1 Componentes* ####

Una UART se compone de:

* Un generador de reloj, que tiene una frecuencia mayor a la velocidad de transmisión de bits de la línea (usualmente 8 veces mayor).
* Registros de corrimiento de entrada y de salida.
* Control de transmisión y de recepción.
* Lógica de control para lectura y escritura.
* Buffers de recepción y transmisión.
* Buffer para datos paralelos.
* Memoria FIFO (First-in, First-out).

#### *1.3.2 Transmisión y recepción de datos seriales* ####

La UART toma una cadena de bits de datos, y transmite cada bit individualmente en una manera secuencial. En el receptor, una segunda UART reconstruye el dato original al reencajar los bits individuales en la cadena completa. Cada UART contiene un *registro de desplazamiento* -shift register-, que representa el método fundamental para la conversión entre datos paralelos y seriales.

La unidad usualmente no genera ni recibe las señales externas, sino que cuenta con distintos dispositivos que traducen los niveles de la entrada al nivel lógico de la UART, y luego pasan del nivel lógico de la UART de nuevo a los niveles de la señal externa.

Algunos esquemas de transmisión de señales no utilizan cables físicos, tales como la fibra óptica, el IrDA (infrarrojo) y el bluetooth a través de su puerto serial SPP -Serial Port Profile-. Oros esquemas utilizan modulación a través de una portadora para transmitir la señal, ya sea a través de medios alámbricos o inalámbricos, como los modems telefónicos y los módulos RF, respectivamente.

#### *1.3.3 Framing de caracteres* ####

Como es de esperarse, la UART utiliza un protocolo serial de comunicación asíncrona. A continuación detallaremos los detalles de este protocolo, a utilizar en el proyecto.

* Cuando no se está transmitiendo información, el estado del sistema es inactivo -*idle*-. Este estado, por tradición, se denomina de alto voltaje, puesto que se transmite un valor de 1 constantemente. 
* Cada palabra se envía con un bit de inicio de nivel bajo, es decir, 0 bits. A continuación, se envían los bits de datos -usualmente 8-, junto con un bit de paridad y uno o más bits de stop con un valor de 1 lógico.

![](http://upload.wikimedia.org/wikipedia/commons/3/3d/Charactercode.png)
![Cuadros donde se indican los caracteres](http://perso.wanadoo.es/pictob/imagenes/formatasic.gif)

#### *1.3.4 Receptor* ####

* La señal de reloj interna de la UART es usualmente 8 veces mayor que la tasa de transmisión de datos.

* El receptor se encarga de mirar el estado de la señal recibida en cada pulso de la señal de CLK interna. Como la línea se mantiene en 1 cuando está inactiva, el receptor busca el principio del bit de start de la nueva code word que va a transmitirse. Una vez detectado el bit comienzo de la palabra, la UART vuelve a muestrear durante un número definido de ciclos de reloj, siempre y cuando en el número de ciclos no se exceda la duración del bit time en la palabra de código. Cuando la UART se ha "convencido" de que el bit registrado es el bit de inicio, se prepara interiormente para el registro de los bits de información.

* Seguidamente, se almacenan los bits de información utilizando un registro de desplazamiento. La UART sabe cuántos bits posee una palabra de código, por lo cual su proceso se detiene luego de almacenar la cantidad preestablecida de bits -en este proyecto, 8, incluyendo bit de paridad-. El bit de stop funciona para verificar la correcta transmisión del dato: almacenados los 8 bits de información, se vuelve a verificar la línea. Si la señal está en 1, se genera una bandera que indica que un nuevo dato está disponible, y se espera a la transmisión de un nuevo dato.

No es necesario que la transmisión entre code words se dé inmediatamente luego del fin del bit de stop de cada palabra de código. Este es el caso en el cual el bit de stop está inmediatamente seguido por el bit de start de una nueva palabra. La comunicación se da de la forma más rápida posible.

![](http://perso.wanadoo.es/pictob/imagenes/asicmaxv.gif)

El caso más general es aquel en el cual se envía un bit de parada al final de cada caracter, y la línea se pone en standby por un intervalo de uno o dos tiempos de bit adicionales. En este caso, la velocidad es menor que la máxima. 

![](http://perso.wanadoo.es/pictob/imagenes/asicmenorv.gif)

*Condiciones especiales del receptor:*

* **Overrun error:** Sucede cuando el receptor no puede procesar la palabra que está llegando antes que la próxima llegue. En este caso, el dato entrante se pierde.

* **Underrun error:** Se presenta cuando el transmisor de la UART ha enviado un dato, pero el buffer de recepción está vacío. Es un error más típico en sistemas síncronos.

* **Framing error:** Es el caso en el cual no se encuentran los bits de start y stop. 

* **Error de paridad:** Luego de la recepción de datos, el bit de paridad indica que el número de 1s en la palabra no coincide con el número original de 1s enviados. 

#### *1.3.5 Transmisor* ####

La operación del transmisor UART es mucho más sencilla que la del receptor puesto que todo el control está a cargo del sistema de transmisión.

* Tan pronto como el dato se almacena en el registro de desplazamiento, se envía una bandera indicándole al sistema de entrada de datos un estado de ocupado para que otro dato no se almacene en el registro de entrada mientras se está enviando la información actual: BUSY = 1.
* El hardware de la UART genera un bit de start. También va realizando los corrimientos necesarios para ir enviando bit por bit el dato. Se puede enviar del MSB al LSB -de izquierda a derecha-, aunque es más usual, por comodidad, enviar el dato del LSB al MSB -de derecha a izquierda-.
* En seguida del envío de todos los bits de información, se genera y envía un bit de paridad.
* Por último, se añaden uno o más bit de stop. 

En este instante, se cambia la bandera de BUSY=0 para leer de nuevo el registro de entrada. Si la información no ha cambiado, se envía un 1 a través de la línea. Si la información cambia, se cambia el valor de la bandera BUSY=1, y el proceso se inicia nuevamente.

#### *1.3.6 Requerimientos* ####

Las UARTs receptoras y transmisoras deben cumplir con los siguientes requerimientos:

* Misma velocidad de transmisión de datos (bps).
* Definir la longitud de la cadena de bits de información. Todas las palabras deben tener igual número de bits.
* La presencia o no de un bit de paridad, par o impar.
* El número de stop bits enviados luego de cada palabra.
* Definición de un *framing error*, el cual considera el caso en el cual la UART receptora recibe un código equivocado. 

---

## **2. Módulo TX-RX** ##

* [xBee](http://www.sigmaelectronica.net/manuals/XBP24_XB24.pdf)

Para la transmisión y recepción de los datos se utilizarán modulos Xbee, que operan a través de radiofrecuencia y mantienen una comunicación serial, los cuales manejan los parámetros adoptados por el estándar  IEEE 802.15.4, proporcionando  bajo costo y bajo uso de potencia para las acciones a realizar.

Por defecto, los módulos RF tienen un modo de operación transparente. En este modo, los módulos actuan como una línea de reemplazo serial, donde todos los datos recibidos por la UART a través del pin DI (Data In) son puestos en lista para la transmisión RF. Caundo los datos son recibidos en RF, estos son enviados al pin DO (Data Out).
Si el módulo no puede transmitir inmediatamente (o no está listo para recibir los datos RF), el dato serial es almacenado en el búfer DI. El dato es empaquetado y enviado en cualquier tiempo dado por el control.

---

## **3. Diagrama de caja negra** ##

![Diagrama de Caja Negra](https://bitbucket.org/wfcastrol/comunicaciones/raw/78d5bddddc228c539e6be8fffea4eaf7559d1762/Diagrama%20General%20Caja%20Negra.png)

---

## **4. Diagrama de bloques internos** ##

![Diagrama de Bloques Internos] (https://bitbucket.org/wfcastrol/comunicaciones/raw/78d5bddddc228c539e6be8fffea4eaf7559d1762/Diagrama%20de%20Bloques%20Internos.png)

---

## **5. Transmisor** ##

###Integrantes###
 * Felipe Galarza (cód. 262005).
 * César Álvarez Parra (cód. 262021).
 * Sergio Andrés Dorado (cód. 262043).

###Funciones###
 * Leer los datos del registro suministrado por el módulo anterior.
 * Añadir bits de START, paridad par y STOP.
 * Enviar una señal serial por cada caracter a través de un módulo de radiofrecuencia a 9600bps.

###Diagrama de flujo###
![Diagrama de Flujo Transmisor] (https://bitbucket.org/wfcastrol/comunicaciones/raw/0f8ea2373e0329bb25927d7ff5d16d4363c88d7c/Transmitter%20Flowchart.png)

###Camino de Datos###
![Camino de Datos Transmisor]
(https://bitbucket.org/wfcastrol/comunicaciones/raw/78d5bddddc228c539e6be8fffea4eaf7559d1762/Transmitter%20DataPath.png)

###Diagrama de Estados###
![Diagrama de Estados Transmisor]
(https://bitbucket.org/wfcastrol/comunicaciones/raw/f2da1dad3fc8a986c7d3935241d2e86d5c1162fe/Transmitter%20State%20Diagram.png)

## **6. Receptor** ##
###Integrantes###
 * David Cruz (cód. 262040).
 * Aura Milena Alba (cód. 262019).
 * Kevin Bello (cód. 261961).
 * Walter Castro (cód. 262036).
###Funciones####
* Recibir una señal serial por cada caracter a través de un módulo de radiofrecuencia.
* Realizar un arreglo de 80 bits (70 bits de palabra y 10 bits de paridad).
* Revisar la paridad de cada caracter, descartando aquellos que tengan errores siendo reemplazados por un caracter "SPACE".
* Remover bits de paridad y enviar al siguiente módulo.

###Diagrama de Flujo##
![Diagrama de Flujo Receptor] (https://bitbucket.org/wfcastrol/comunicaciones/raw/c45180000a20748a2f77d8d925c46c26dd8e9b55/receiver.png)

### Camino de Datos ###
![Camino de Datos Receptor] (https://bitbucket.org/wfcastrol/comunicaciones/raw/c45180000a20748a2f77d8d925c46c26dd8e9b55/DataPathReciever.png)

###Diagrama de Estados###
![Diagrama de estados Receptor] (https://bitbucket.org/wfcastrol/comunicaciones/raw/1503ccce021c4d7acb86c88904c35cb6a68535db/Receiving_state_machine_2.PNG)

###Codigo###

####Módulo de Counter####

       always @ (posedge clk)
       begin : COUNTER// Block Name
            
 		if (reset == 1'b1) begin
    		char_count <= 3'b111;
  		str_count <= 4'b1010;
  		end
  		else begin

   			 if (new_bit == 1'b0) begin
    			char_count <= 3'b111;
  			end
  			else begin
   			char_count <= char_count - 1;
  			end

			if (char_count == 3'b000) begin
  			char_count <= 3'b111;
  			end
  			else begin   			
  			end

		        if (new_char == 1'b0) begin
  			str_count <= 4'b1010;
  			end
  			else begin
   			str_count <= str_count - 1;
  			end

			if (str_count == 4'b0000) begin
  			str_count <= 4'b1010;
  			end
  			else begin   			
  			end

  		end
        end

####Módulo de Parity####

	always @(ParityRecieved)begin
		Parity = ~((^char) ^ ParityRecieved);
	end

####Módulo de charComparator####

	always @(char_count)begin
		CharReady = (~| char_count);
	end

####Módulo de stringComparator####
	
	always @(string_count)begin
		StringReady = (~| string_count);
	end

####Módulo de Char####

        always @ (posedge clk) begin
		if (writeChar== 1'b1) begin
			char<={rxd, char[6:1]};
			new_bit <= 1'b1;
	        end
        
                 if(error==1'b1)
	                 char<=7'b0000010;

                 if(reset==1'b1)
		         char<=7'b0000000;
	  end

####Módulo de String####

      always @ (posedge clk) begin
		if (writeString== 1'b1) begin
			string<={char, string[69:7]};
			new_char <= 1'b1;
		end
	
		if(reset==1'b1)
			string<=70'b0;
	end
####Módulo de Control####

        always @(posedge clk) begin

           if (CharReady == 1'b1)
             if (rxd == 1'b0) begin
               if (check == 1'b1) begin
                 PossibleStart <=1'b0;
                 WriteChar <=1'b1;
               end 
             if (check == 1'b0)
               PossibleStart <= 1'b1;
           end
	   
           if (Parity == 1'b0)
             error <= 1'b1;
           else
             error <=1'b0;
	   
           if (CharReady == 1'b0)
             WriteChar <= 1'b1;
           else
             WriteChar = 1'b0;
	   
           if (StringReady == 1'b0)
             WriteString <= 1'b1;
           else begin
             WriteString <= 1'b0;
             ready <= 1'b1;
           end
	   
           if (reset == 1'b1) begin
             ready <= 1'b0;
             error <= 1'b0;
             WriteChar <= 1'b0;
             WriteString <= 1'b0;
             PossibleStart <= 1'b0;
           end   
        end

## **Referencias** ##
[1] [La comunicación serie](http://perso.wanadoo.es/pictob/comserie.htm#la_transmision_asincrona). Dispositivos Lógicos Microprogramables.

---